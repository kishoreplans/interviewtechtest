package starter.petstore;

import com.google.gson.JsonObject;

import io.restassured.RestAssured;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import starter.WebServiceEndPoints;

public class PetStore {
	 @Step("Add a new pet to petstore")
	    public void addPet(JsonObject pet) {
		 SerenityRest.given()
	                .contentType("application/json")
	                .header("Content-Type", "application/json")
	                .body(pet.toString())
	                .when()
	                .post(WebServiceEndPoints.PET.getUrl());
	    }
	 
	 @Step("Get a pet from petstore")
	    public void getPet(String suburl) {
		 SerenityRest.given()
	                .contentType("application/json")
	                .header("Content-Type", "application/json")
	                .when()
	                .get(WebServiceEndPoints.PET.getUrl()+"/"+suburl);
	    }
	 
	 @Step("update a pet to petstore")
	    public void updatePet(JsonObject pet) {
		 SerenityRest.given()
	                .contentType("application/json")
	                .header("Content-Type", "application/json")
	                .body(pet.toString())
	                .when()
	                .put(WebServiceEndPoints.PET.getUrl());
	    }
	 
	 @Step("delete a pet from petstore")
	    public void deletePet(String petid) {
		 SerenityRest.given()
	                .contentType("application/json")
	                .header("Content-Type", "application/json")
	                .when()
	                .delete(WebServiceEndPoints.PET.getUrl()+"/"+petid);
	    }
	 
}
