package starter;

public enum WebServiceEndPoints {

    PET("https://petstore.swagger.io/v2/pet");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
