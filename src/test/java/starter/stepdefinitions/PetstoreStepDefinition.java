package starter.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import starter.petstore.PetStore;


public class PetstoreStepDefinition {

	public static String petid;
	int statuscode;
	String petbody;
	JsonObject req1 = new JsonObject();
	Response response;
	
	@Steps
	PetStore petStore;
	

	@Given("a pet needs to be created")
	public void a_pet_needs_to_be_created() {
	req1.addProperty("id", 0);
	req1.addProperty("name", "test");
	JsonObject req2 = new JsonObject();
	req2.addProperty("id", 0);
	req2.addProperty("name", "test 2");
	req1.add("category", req2);
	JsonArray req3 = new JsonArray();
	req3.add("string");
	req1.add("photoUrls", req3);
	JsonArray req4 = new JsonArray();
	JsonObject req5 = new JsonObject();
	req5.addProperty("id", 0);
	req5.addProperty("string", "test2");
	req4.add(req5);
	req1.add("tags", req4);
	req1.addProperty("status", "available");

	}


	@When("we creat a pet by post endpoint")
	public void we_creat_a_pet_by_post_endpoint() {
		petStore.addPet(req1);
	}
	@Then("a pet is created")
	public void a_pet_is_created() {
	
		 restAssuredThat(lastResponse -> lastResponse.statusCode(equalTo(200)));
		 petid = SerenityRest.lastResponse().getBody().jsonPath().getString("id");
		 System.out.println(petid);
	}
	
	@Given("a pet exists")
	public void a_pet_exists() {
	 
	}

	@When("find by pet id")
	public void find_by_pet_id() {
		System.out.println(petid);
		petStore.getPet(petid);
	}
	@Then("retrieves the pet info")
	public void retrieves_the_pet_info() {
	  
		restAssuredThat(lastResponse -> lastResponse.statusCode(equalTo(200)));
	}
	
	
	@When("we update by petid")
	public void we_update_by_petid() {
		req1.remove("id");
		req1.addProperty("id", petid);
		petStore.updatePet(req1);
	 
	}

	@Then("update happend successfully.")
	public void update_happend_successfully() {
		restAssuredThat(lastResponse -> lastResponse.statusCode(equalTo(200)));
	}

	@Given("a pet exists by created")
	public void a_pet_exists_by_created() {
	    
	}
	
	@Given("a pet exists on application")
	public void a_pet_exists_on_application() {
	}


	@When("we get search by status")
	public void we_get_search_by_status() {
		petStore.getPet("findByStatus?status=available");
	}
	
	@Then("results should be displayed")
	public void results_should_be_displayed() {
		restAssuredThat(lastResponse -> lastResponse.statusCode(equalTo(200)));
	}
	
	@When("we delete pet by id")
	public void we_delete_pet_by_id() {
		petStore.deletePet(petid);
	}


	@Then("petid deleted")
	public void petid_deleted() {
		restAssuredThat(lastResponse -> lastResponse.statusCode(equalTo(200)));
	}
}
