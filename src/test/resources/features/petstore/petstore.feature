Feature: Test the pet store endpoints

Scenario: Post end point


Given a pet needs to be created
When we creat a pet by post endpoint
Then a pet is created

Scenario: Get end point

Given a pet exists
When find by pet id
Then retrieves the pet info

Scenario: PUT end point

Given a pet exists by created
When we update by petid
Then update happend successfully.

Scenario: Get by Status end point

Given a pet exists on application
When we get search by status
Then results should be displayed

Scenario: Delete end point

Given a pet exists on application
When we delete pet by id
Then petid deleted